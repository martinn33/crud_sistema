<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\DB;


class AlumnosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('alumnos')->insert([
            'name' => 'martin',
            'apellido_paterno' => 'estrada',
            'apellido_materno' => 'contreras',
            'semestre' => 1,
            'grupo' => A,
        ]);
    }
}